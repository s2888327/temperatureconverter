package nl.utwente.di.tempConverter;

public class Converter {

    public double getTempInFahrenheit(String temp) {
        double val = Double.parseDouble(temp);
        return (val * 1.8) + 32;
    }
}
