package nl.utwente.di.tempConverter;


import org.junit.jupiter.api.Assertions ;
import org.junit.jupiter.api.Test;
/**
∗ Tests the Quoter
*/
public class TestConverter {
    @Test
    public void testTemp () throws Exception {
    Converter converter = new Converter ( ) ;
    double temp = converter.getTempInFahrenheit("25");
    Assertions.assertEquals(77.0,temp);
    }

}